#include <stdio.h>
#include <stdlib.h>
#include <string>

typedef struct sign_node
{
	unsigned char sign;
	int  count;
	short flag;
	char ciper[64];
	struct sign_node * left_node;
	struct sign_node * right_node;

} SYM,*NODE_POINT;

struct fast_node
{
	unsigned char sign;
	unsigned int bit_mask;
	int offset;
};


extern FILE *file_decoder;
extern FILE *file_exit;
extern int count_regulator;

void create_node_tree(NODE_POINT *root,unsigned char sign); // ������� ���������� �� �������
int create_signs_tree(NODE_POINT *root,FILE *file);// ������� �������� ������ �������� ��������� �� ����� � �� ��������
void create_counts_tree(NODE_POINT *root,unsigned char sign,int count,NODE_POINT left,NODE_POINT right);//������ ���������� ��� �������� �������� �� ��������� � �� �� ��������
void nulling_tree(NODE_POINT *root);// ������� ��������� ������ ������
void rebild_tree(NODE_POINT *root,NODE_POINT *new_root);// ������� ������������ ������
NODE_POINT destribution(NODE_POINT *root); // ������� ������������� ���������� �� ����� � ������ ������
void filling_massive(NODE_POINT *massive_to_filling,NODE_POINT root_of_tree);
void prepear_exit_file(NODE_POINT *massive); // ������ ������������ �������
int strlen_SYM(NODE_POINT *massive); // ������� ����������� ������ ������� �� �����������
void sort_massive(NODE_POINT *massive,int size_mas);// ���������� ������� ��� ���������� ������
void del_elm_mas(NODE_POINT *massive,int elm);// �������� ��������� ��������� �� �������� �������� �� ���������
NODE_POINT create_haffman_node(NODE_POINT left,NODE_POINT right); // �������� ������ ���� ������ ��������
NODE_POINT building_haffman(NODE_POINT *massive); // �������� ����� ��������
void create_ciper(NODE_POINT root,char *sign); // �������� ������� ����� ��������
void print_haf(NODE_POINT root); // ���������� �������
NODE_POINT seek_sign(NODE_POINT root,unsigned char sign); // ����� � ����� �������� ���������� �������
int create_haffman_output(NODE_POINT root, FILE *file,int signs_before); // �������� ������� �����
void decoder(NODE_POINT root,long long int teil,FILE *file);// �������������� ������� �����
void file_decomp_ini(FILE *file_in,FILE *file_out); // �������������� ����� ����� �����������
void fill_fast_mas(NODE_POINT root, fast_node *mass);
int get_counts(NODE_POINT massive[258],FILE *file_from);
void fill_fast_mas_decod(NODE_POINT root, fast_node *mass);
int dec_rec(int bit_mask,FILE *file,NODE_POINT root);