#include "fooditem.h"

extern FoodBase foodItem::baseFood;

foodItem::foodItem(QGraphicsScene *inScene, QPoint location, qint32 maxfood, qreal rad)
    :maxFood(maxfood)
{
    m_food = rand() % maxFood;
    m_scene = inScene;
    m_rad = rad;

    qreal one = (255 * 0.01);
    qreal perfood = static_cast<double>(m_food) / static_cast<double>(maxFood)  * 100;
    qreal perAlpha = one *  perfood;
    if(perAlpha < 51) perAlpha +=50;
    foodColor = QColor(0,128,0,perAlpha);

    m_scene->addItem(this);

    setPos(location);

    baseFood.addFood(this);
    lastFeed = QTime::currentTime();
    timeFoodEnd = QTime::currentTime();

    timeAraise = 20;
    isInBase = true;
}

foodItem::~foodItem()
{
    baseFood.removeFood(this);

    removeAllFishes();
}

QRectF foodItem::boundingRect() const
{
    return QRectF(-m_rad,-m_rad,m_rad * 2,m_rad * 2);
}

void foodItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawText(boundingRect(),QString::number(m_food));

    painter->setPen(Qt::lightGray);
    painter->setBrush(foodColor);

    painter->drawEllipse(boundingRect());
}

void foodItem::addNewloader(Fish *loader)
{
    if(m_food < 0) return;
    m_fishes.push_back(loader);
    loader->setMoveTarget(pos());
    loader->setFoodTarget(this);
}

void foodItem::removeLoader(Fish *loader)
{
    for(int i = 0 ; i < m_fishes.size();i++)
    {
        if(m_fishes.at(i) == loader)
        {
            m_fishes.at(i)->resetMoveTarget();
            loader->resetFoodTarget();
           m_fishes.removeAt(i);
            break;
        }
    }
}

bool foodItem::isHaveSomeFood()
{
    if(m_food > 1)
        return true;
    else
        return false;
}

void foodItem::FeedFish(Fish *fish)
{
    fish->incSatiety(5);
    m_food -=5;
}

void foodItem::advance(int phase)
{
    if(phase == 0) return;
//    чем прозрачнее облачко с едой тем её осатётся меньше
//    альфа канал берется в процентах от оставшейся еды
    qreal one = (255 * 0.01);
    qreal perfood = static_cast<double>(m_food) / static_cast<double>(maxFood)  * 100;
    qreal per = one * perfood;

    if(per < 51) per += 50;
    foodColor.setAlpha(per);

    if(lastFeed.msecsTo(QTime::currentTime()) > 1000)
    {
    feedMyFishes();
    lastFeed = QTime::currentTime();
    update(boundingRect());

    }

    if(m_food < 1 && isInBase)
    {
        removeAllFishes();
        baseFood.removeFood(this);
        isInBase = false;
        timeFoodEnd = QTime::currentTime();
//        m_scene->removeItem(this);
    }
    else if((!isInBase) && (timeFoodEnd.secsTo(QTime::currentTime()) > timeAraise) && (m_food < 5))
    {
        m_food = rand() % maxFood;
        baseFood.addFood(this);
        isInBase = true;
        timeFoodEnd = QTime::currentTime();
    }
}

void foodItem::feedMyFishes()
{
    for(int i = 0 ; i < m_fishes.size();i++)
    {
        if(m_fishes.at(i)->getSatiety() > Fish::maxSatiety)
        {
            removeLoader(m_fishes.at(i));
            continue;
        }
        m_fishes.at(i)->incSatiety(10);
        m_food -= 10;
    }
}

void foodItem::removeAllFishes()
{
    while(!m_fishes.isEmpty())
    {
        m_fishes.front()->resetFoodTarget();
        m_fishes.front()->resetMoveTarget();
        m_fishes.pop_front();
    }
}
