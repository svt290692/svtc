#include "fishcalctodo.h"

extern QSemaphore world_sem;


FishCalcToDo::FishCalcToDo(Fish *infish, QObject *parent) :
    QThread(parent)
{
    m_fish = infish;
    isFoodExists = false;
    Stop = false;
    nearestFood = 0;
}

void FishCalcToDo::run()
{
    if(Stop)
    {
        return;
    }


    world_sem.release();//освобождение одной еденицы емафоры

    if(m_fish->isHaveTarget && m_fish->m_FoodTarget == 0)
    moveTo(m_fish->m_moveTarget);
    else if(m_fish->isHungry)
    {
        FindFood();
        if(!isFoodExists) Relax();
    }
    else
    Relax();

    if(m_fish->endBerry.secsTo(QTime::currentTime()) > m_fish->noReproduceTimeSec)
        Reproduce();

    calcFoodAction();
    decrementSatiety(); //убавлениесытоси просиходит кажую итерацию действия рыбки
    calcSpeed();
    m_fish->m_startAction = QTime::currentTime();
//    if(isFoodZone())

//    calcSpeed();// калькуляция скорости от сытости чем сытнее тем быстрее рыбка

    world_sem.acquire();// убавление одной едениц семафоры
    //пляска с семафорой для того чтобы пока какой либо потк управляющий физическими движениями рыб
    //еще работал то перед следующим шагом расч1та мира мир подождал бы пока все потоки закончат свою работу

}

quint32 FishCalcToDo::getSemCount() const
{
    return world_sem.available();

}

void FishCalcToDo::Relax()
{
    QPointF relax;
    relax.setX(m_fish->pos().x() + (20-(rand()%40)));
    relax.setY(m_fish->pos().y() + (20-(rand()%40)));

    moveTo(relax);
}


void FishCalcToDo::FindFood()
{
    nearestFood = (foodItem*)foodItem::baseFood.nearestFoodFrom(m_fish->pos().toPoint());
    if(nearestFood == 0)
    {
        isFoodExists = false;
        m_fish->isHaveTarget = false;
        m_fish->m_FoodTarget = 0;
        m_fish->m_moveTarget = QPointF();
        return;
    }
    else
    {
        isFoodExists = true;
        m_fish->isHaveTarget = true;
        m_fish->m_moveTarget = nearestFood->pos();
    }
    //    qreal velocityInc;
//    if(m_fish->m_speedOrder == Fish::low) velocityInc = 0.5;
//    if(m_fish->m_speedOrder == Fish::normal) velocityInc = 1;
//    if(m_fish->m_speedOrder == Fish::height) velocityInc = 1.5;

//    qreal disX = nearestFood->pos().x() - m_fish->pos().x();
//    qreal disY = nearestFood->pos().y() - m_fish->pos().y();

//    m_fish->m_body->SetLinearVelocity(b2Vec2(disX * velocityInc,disY * velocityInc));
}

void FishCalcToDo::Reproduce()
{
    if(m_fish->isberry && m_fish->startBerry.secsTo(QTime::currentTime()) > m_fish->ReproduceTimeSec)
    {
        m_fish->goingBirth = true;
        m_fish->endBerry = QTime::currentTime();
        m_fish->isberry = false;
    }
    else if(!m_fish->isberry)
    {
        if(isFishNear())
        {
            m_fish->isberry = true;
            m_fish->startBerry = QTime::currentTime();
        }
    }

}

void FishCalcToDo::Feed(foodItem *food)
{
    if(food)
    {
    food->FeedFish(m_fish);
    }
}

void FishCalcToDo::moveTo(QPointF target, quint32 mulSpeed)
{
    if(target == QPointF()) return;

    qreal velocityIncrement;
    if(m_fish->m_speedOrder == Fish::low) velocityIncrement = 0.5 * mulSpeed;
    if(m_fish->m_speedOrder == Fish::normal) velocityIncrement = 1 * mulSpeed;
    if(m_fish->m_speedOrder == Fish::height) velocityIncrement = 1.5 * mulSpeed;

    qint32 Xvelocity = target.x() - m_fish->pos().x();
    qint32 Yvelocity = target.y() - m_fish->pos().y();

    m_fish->m_body->SetLinearVelocity(b2Vec2(Xvelocity * velocityIncrement,Yvelocity * velocityIncrement));
}


void FishCalcToDo::calcFoodAction()
{
    if(m_fish->isHaveTarget && m_fish->m_FoodTarget == 0 && checkPos(m_fish->m_moveTarget,50))
    {
        if(nearestFood)
        {
        static_cast<foodItem*>(nearestFood)->addNewloader(m_fish);
        }
    }
    else if(m_fish->m_FoodTarget != 0 && !checkPos(m_fish->m_FoodTarget->pos(),50))
    {
        static_cast<foodItem*>(m_fish->m_FoodTarget)->removeLoader(m_fish);
    }
    else if(m_fish->isHaveTarget && nearestFood)
    {
        if(!static_cast<foodItem*>(nearestFood)->isHaveSomeFood())
        {
        m_fish->isHaveTarget = false;
        m_fish->m_moveTarget = QPointF();
        }
    }
}

bool FishCalcToDo::checkPos(QPointF pos, quint32 error)
{
    if(m_fish->pos().x() > (pos.x() - error) && m_fish->pos().x() < (pos.x() + error)
        && m_fish->pos().y() > (pos.y() - error) && m_fish->pos().y() < (pos.y() + error))
        return true;
        else
        return false;
}

bool FishCalcToDo::isFishNear()
{
 \
    return m_fish->isItemsNearest;
}

void FishCalcToDo::calcSpeed()
{
    quint32 satiety = m_fish->satiety;

    if(satiety > 80) m_fish->m_speedOrder = Fish::height;
    else if(satiety > 40) m_fish->m_speedOrder = Fish::normal;
    else m_fish->m_speedOrder = Fish::low;

}

void FishCalcToDo::decrementSatiety()
{
    if(m_fish->satiety > 2)
    {
        if(m_fish->satiety > Fish::maxSatiety)
        {
            m_fish->satiety = Fish::maxSatiety;
            return;
        }
        m_fish->satiety -= 2;
        if(m_fish->satiety < 40) m_fish->isHungry = true;
        else m_fish->isHungry = false;
    }
    else Die();

}

void FishCalcToDo::Die()
{
    m_fish->goingDie = true;
}


//bool FishCalcToDo::isFoodZone()
//{
//    if(getCurFood() == 0)return false;
//    else return true;
//}

//foodItem *FishCalcToDo::getCurFood()
//{

//}
