#ifndef FOODITEM_H
#define FOODITEM_H

#include <QtCore>
#include <QtGui>
#include <QGraphicsItem>
#include <QList>
#include <QTime>
#include <QObject>
#include "foodbase.h"
#include "fishcalctodo.h"

class FoodBase;
class Fish;
class FishCalcToDo;

class foodItem : public QGraphicsItem
{

//    friend class FishCalcToDo;
//    friend class Fish;
public:
    foodItem(QGraphicsScene * inScene,QPoint location,qint32 maxfood =1000,qreal rad = 50);
    ~foodItem();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void addNewloader(Fish * loader);
    void removeLoader(Fish * loader);

    bool isHaveSomeFood();

    void FeedFish(Fish*fish);

    static FoodBase baseFood;
protected:

    void advance(int phase);
private:

    QGraphicsScene *m_scene;
    QColor foodColor;
    qreal m_rad;
    qint32 m_food;
    const quint32 maxFood;

    QList<Fish*> m_fishes;
    QTime lastFeed;
    QTime timeFoodEnd;
    quint32 timeAraise;
    bool isInBase;
    void feedMyFishes();
    void removeAllFishes();
};
#endif // FOODITEM_H
