#include "predatorcalctodo.h"

//extern FoodBase Fish::PreyBase;

predatorCalcToDo::predatorCalcToDo(Fish *infish, QObject *parent)
    :FishCalcToDo(infish,parent)
{
    isFoodExists = true;
}

void predatorCalcToDo::run()
{
    if(Stop)
    {
        return;
    }


    world_sem.release();//освобождение одной еденицы емафоры

    if(m_fish->isHungry)
    {
        if(!hunt())
            Relax();
    }
    else
        Relax();

    decrementSatiety();
    calcSpeed();
    calcFoodAction();
    Reproduce();

    m_fish->m_startAction = QTime::currentTime();

    world_sem.acquire();
}

void predatorCalcToDo::FindFood()
{
    Fish *Food = static_cast<Fish*>(Fish::PreyBase.nearestFoodFrom(m_fish->pos()));
    if(Food == 0 || Fish::PreyBase.distanceFrom(m_fish->pos(),Food->pos()) > predator::rangeView)
        isFoodExists = false;
    else
    {
        isFoodExists = true;
        m_fish->m_moveTarget = Food->pos();
        m_fish->m_FoodTarget = Food;
    }
}

void predatorCalcToDo::Reproduce()
{
    if(static_cast <predator*>(m_fish)->getEatCount() >= predator::eatToReproduce)
    {
        m_fish->goingBirth = true;
        static_cast <predator*>(m_fish)->resetEated();
    }
}

void predatorCalcToDo::decrementSatiety()
{
    if(m_fish->satiety > 4)
    {
        if(m_fish->satiety > predator::maxPredatorSatiety)
        {
            m_fish->satiety = predator::maxPredatorSatiety;
            return;
        }
        m_fish->satiety -= 4;
        if(m_fish->satiety < 100) m_fish->isHungry = true;
        else m_fish->isHungry = false;
    }
    else Die();
}

void predatorCalcToDo::calcFoodAction()
{
    if(isFoodExists && m_fish->m_FoodTarget)
    {

        qreal dist = Fish::PreyBase.distanceFrom(m_fish->pos(),m_fish->m_FoodTarget->pos());
        if(dist < 21)
        {
        eatFish(static_cast<Fish*>(m_fish->m_FoodTarget));
        m_fish->m_moveTarget = QPointF();
        m_fish->m_FoodTarget = 0;
        }
    }
}


bool predatorCalcToDo::hunt()
{
    FindFood();
    if(isFoodExists && m_fish->m_FoodTarget)
    {
        moveTo(m_fish->m_FoodTarget->pos(),3);
        return true;
    }
    else return false;
}

void predatorCalcToDo::eatFish(Fish *fish)
{
    fish->goingDie = true;

    static_cast <predator*>(m_fish)->incEatCount();

    m_fish->satiety += 40;
}
