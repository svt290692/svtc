#ifndef OCEAN_H
#define OCEAN_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QVector>
#include <QGraphicsItem>
#include <QGraphicsItem>
#include <QPainter>
#include <Box2D/Box2D.h>

namespace Ui {
class Ocean;
}

class Ocean : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Ocean(QWidget *parent = 0);
    ~Ocean();
public slots:
    void moveItems();
signals:
    void makeStep();
    
private slots:

    void on_HS_scale_sliderMoved(int position);


    void on_action_addFish_triggered();

protected:
    void mousePressEvent(QMouseEvent *event);
private:
    qint32 columnsMax;
    qint32 stringsMax;

    QGraphicsScene *scene;
    b2World *m_world;

    QPoint zeroCoord;
    qint32 sizeCells;

    qreal OceanScale;

    QVector < QVector < QPoint > > positions;

    enum modes
    {
        noMode,addFish,addpredator,addFood,remove
    }m_mod;

    Ui::Ocean *ui;
};
void sleepFor(qint64 milliseconds);

#endif // OCEAN_H
