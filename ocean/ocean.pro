#-------------------------------------------------
#
# Project created by QtCreator 2014-05-22T17:53:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ocean
TEMPLATE = app


SOURCES += main.cpp\
        ocean.cpp \
    fooditem.cpp \
    physics_circle.cpp \
    fish.cpp \
    fishcalctodo.cpp \
    foodbase.cpp \
    predator.cpp \
    predatorcalctodo.cpp

HEADERS  += ocean.h \
    phisics_gaphicsitem.h \
    fooditem.h \
    physics_circle.h \
    fish.h \
    fishcalctodo.h \
    foodbase.h \
    predator.h \
    predatorcalctodo.h

FORMS    += ocean.ui

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../Box2D_v2.2.1/Build/Box2D/release/ -lBox2D
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../Box2D_v2.2.1/Build/Box2D/debug/ -lBox2D
#else:unix:!macx:!symbian: LIBS += -L$$PWD/../../../Box2D_v2.2.1/Build/Box2D/ -lBox2D

#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Build/Box2D
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Rope
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Dynamics/Contacts
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Dynamics/Joints
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Dynamics
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Common
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Collision
#INCLUDEPATH += $$PWD/../../../Box2D_v2.2.1/Box2D/Collision/Shapes
#DEPENDPATH += $$PWD/../../../Box2D_v2.2.1/Build/Box2D

#win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../Box2D_v2.2.1/Build/Box2D/release/Box2D.lib
#else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../Box2D_v2.2.1/Build/Box2D/debug/Box2D.lib
#else:unix:!macx:!symbian: PRE_TARGETDEPS += $$PWD/../../../Box2D_v2.2.1/Build/Box2D/libBox2D.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/release/ -lBox2D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/debug/ -lBox2D
else:unix:!macx:!symbian: LIBS += -L$$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/ -lBox2D

INCLUDEPATH += $$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D
INCLUDEPATH += $$PWD/../../Box2D_v2.3.0/Box2D
DEPENDPATH += $$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D

win32:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/release/Box2D.lib
else:win32:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/debug/Box2D.lib
else:unix:!macx:!symbian: PRE_TARGETDEPS += $$PWD/../../Box2D_v2.3.0/Box2D/Build/Box2D/libBox2D.a

RESOURCES += \
    fish.qrc
