#include "predator.h"

predator::predator(b2World *inWorld, QGraphicsScene *inScene, qreal x, qreal y)
    :Fish(inWorld,inScene,x,y,false)
{
    m_picture  = QImage(":/predators/predator.png");
    m_calc = new predatorCalcToDo(this,m_scene);
    eatFishCount = 0;
}

QRectF predator::boundingRect() const
{
    return Fish::boundingRect();
}

void predator::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Fish::paint(painter,option,widget);
}

void predator::advance(int phase)
{

    if(m_FoodTarget && Fish::PreyBase.distanceFrom(pos(),m_FoodTarget->pos()) < 21)
    {
        static_cast<Fish*>(m_FoodTarget)->setDie(true);
        incEatCount();
        m_FoodTarget = 0;
        satiety += 40;
    }

    Fish::advance(phase);

}

void predator::birth()
{
    new predator(m_world,m_scene,pos().x(),pos().y());
    goingBirth = false;
}
