#ifndef FOODBASE_H
#define FOODBASE_H

#include <QVector>
#include <QVectorIterator>
#include <qmath.h>
#include <QObject>
#include <QtCore>
#include <QTimer>
#include "fooditem.h"

class foodItem;

class FoodBase: public QObject
{
    Q_OBJECT
public:
    FoodBase(QObject *Parent = 0);

    void addFood(QGraphicsItem * food);
    void removeFood(QGraphicsItem *food);
    QGraphicsItem *nearestFoodFrom(QPointF pos);

    static quint32 distanceFrom(QPointF pos1,QPointF pos2);

private:
    QVector<QGraphicsItem*> base;
};

#endif // FOODBASE_H
