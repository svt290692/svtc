#include "fish.h"
#include "predator.h"
#include <QDebug>

extern class FoodBase Fish::PreyBase;

Fish::Fish(b2World * inWorld, QGraphicsScene *inScene, qreal x, qreal y, bool isPrey)
    :physics_circle(inWorld,inScene,10,x,y)
{
    QStringList images;
    images.push_back(":/fish/redFIsh.png");
    images.push_back(":/fish/greenFish.png");
    images.push_back(":/fish/blueFish.png");
    images.push_back(":/fish/blueFish2.png");
    images.push_back(":/fish/grayFish.png");
    images.push_back(":/fish/grayFish2.png");
    images.push_back(":/fish/orangeFish.png");
    images.push_back(":/fish/yellowFish.png");

    m_picture = QImage(images.at(rand() % 8));

    m_FoodTarget = 0;
    m_moveTarget.setX(0);
    m_moveTarget.setY(0);
    m_speedOrder = normal;
    satiety = 50;

    goingDie = false;

    goingDieTimer;
    skeletTime = 4;
    isSkelet = false;
    goingBirth = false;
    isHungry = false;

    isberry = false;
    isItemsNearest = false;

    endBerry = startBerry = QTime::currentTime();
    ReproduceTimeSec = 15;
    noReproduceTimeSec = 20;

    m_startAction = QTime::currentTime();

    m_body->SetLinearDamping(0.4);

    this->isPrey = isPrey;
    if(isPrey)
    {
        m_calc = new FishCalcToDo(this,m_scene);
        PreyBase.addFood(this);
    }
    else m_calc = 0;
}

Fish::~Fish()
{
    if(m_calc)
    delete m_calc;

    m_world->DestroyBody(m_body);
    PreyBase.removeFood(this);
}

QRectF Fish::boundingRect() const
{
    return physics_circle::boundingRect();
}

void Fish::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QImage image;
    qreal x =  m_body->GetLinearVelocity().x;
//    qreal y =  m_body->GetLinearVelocity().y;
//    qreal xy_x = x;
//    qreal xy_0 = qSqrt(y*y + x*x);

//    qreal cos = (-(x*x)+(xy_0 * xy_0)-(-(xy_x * xy_x))) / (2 * x * xy_0);

//    qreal ang = qAcos(cos) - 1.2;


    if(x < 0)
        image = m_picture.mirrored(true,false);
    else
        image = m_picture;

    painter->drawImage(boundingRect(),image);

//    painter->drawText(boundingRect(),QString::number(satiety));
}

void Fish::setFoodTarget(QGraphicsItem *targ)
{
    m_FoodTarget = targ;
}

void Fish::setMoveTarget(QPointF pos)
{
    m_moveTarget = pos;
    if(pos == QPointF()) isHaveTarget = false;
}

//bool Fish::isFree()
//{
//    if(isFoodSeek || isReproduceSeek) return false;
//    else return true;
//}

quint32 Fish::ActionSecond() const
{
    return m_startAction.secsTo(QTime::currentTime());
}

void Fish::advance(int phase)
{
    if(goingDie)
    {
        physics_circle::advance(phase);
        remove();
        return;
    }
    if(m_startAction.msecsTo(QTime::currentTime()) >  (2000 + (rand() % 5000)))
    {
        if(!((nearestItems = getNearestList()).isEmpty())) isItemsNearest = true;
        else isItemsNearest = false;

        if(m_calc)
        m_calc->start();
    }

    if(goingBirth)
    {
        birth();
    }
    physics_circle::advance(phase);
}

void Fish::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    QMessageBox mb;
    QString str;
    if(isberry)
    {
        str += "berry\n";

        str += "start ber = ";
        str += startBerry.toString();
        str += "\n";

        str += "end ber = ";
        str += endBerry.toString();
        str += "\n";

        str += "Remaning time beery = ";
        str += QString::number(ReproduceTimeSec - startBerry.secsTo(QTime::currentTime()));
        str += "\n";

    }
    else str += " not berry\n";


    str += "hungry = ";
    str += QString::number(satiety);
    str +="\n";

    if(isHaveTarget)
    {
        str += "move to    x = ";
        str += QString::number(m_moveTarget.x());
        str += "    y = ";
        str += QString::number(m_moveTarget.y());
        str +="\n";
    }
    else
    {
        str += "Relax";
    }
    mb.setText(str);
    mb.exec();
    QGraphicsItem::mouseDoubleClickEvent(event);
}

void Fish::remove()
{
    if(isSkelet == false)
    {
        m_picture = QImage(":/other/skelet.png");
        isSkelet = true;
        goingDieTimer = QTime::currentTime();

        PreyBase.removeFood(this);

        if(m_calc)
        {
            while(m_calc->isRunning())
            {
                sleepFor(2);
            }
            m_calc->Stop = true;
        }
    }
    else if(goingDieTimer.secsTo(QTime::currentTime()) > skeletTime)
    {
        m_scene->removeItem(this);
    }

    m_body->SetLinearVelocity(b2Vec2(0,-0.1));
}

void Fish::birth()
{
    if(isPrey)
    new Fish(m_world,m_scene,pos().x(),pos().y());
    else
    new predator(m_world,m_scene,pos().x(),pos().y());

    goingBirth = false;
}

QList<QGraphicsItem *> Fish::getNearestList()
{
    return m_scene->collidingItems(this);
}
