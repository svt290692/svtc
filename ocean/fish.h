#ifndef FISH_H
#define FISH_H
#include <QTime>
#include <qmath.h>
#include <QMessageBox>
#include "physics_circle.h"
#include "fishcalctodo.h"
//#include "predatorcalctodo.h"

class FishCalcToDo;
class predatorCalcToDo;
class foodItem;
class FoodBase;
void sleepFor(qint64 milliseconds);

class Fish : public physics_circle
{
    friend class FishCalcToDo;
    friend class predatorCalcToDo;
public:
    Fish(b2World * inWorld, QGraphicsScene *inScene, qreal x, qreal y, bool isPrey = true);
    ~Fish();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setDie(bool isit){goingDie = isit;}
    void setHaveTarget(bool isit){isHaveTarget = isit;}

    void setFoodTarget(QGraphicsItem *targ);
    void resetFoodTarget(){m_FoodTarget = 0;isHaveTarget = false;}


    void setMoveTarget(QPointF pos);
    void resetMoveTarget(){m_moveTarget = QPointF(); isHaveTarget = false;}

    void decSatiety(quint32 sat){satiety -= sat;}
    void incSatiety(quint32 sat){satiety += sat;}
    quint32 getSatiety()const{return satiety;}


    quint32 ActionSecond()const;

    static FoodBase PreyBase;
    static const quint32 maxSatiety = 100;
protected:
    void advance(int phase);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void remove();
    void birth();

    QList<QGraphicsItem*> getNearestList();

    QImage m_picture;

    quint32 satiety;

    bool isHungry;

    bool isHaveTarget;
    QPointF m_moveTarget;
    QGraphicsItem *m_FoodTarget;

    bool isberry;
    QTime startBerry;
    QTime endBerry;
    quint32 noReproduceTimeSec;
    quint32 ReproduceTimeSec;

    enum speed_types
    {
        low,normal,height
    }m_speedOrder;


    bool goingDie;
    bool isSkelet;
    QTime goingDieTimer;
    quint32 skeletTime;

    bool goingBirth;
    QTime m_startAction;

    bool isItemsNearest;
    QList<QGraphicsItem*> nearestItems;

    FishCalcToDo * m_calc;
    bool isPrey;
};

#endif // FISH_H
