#ifndef PREDATORCALCTODO_H
#define PREDATORCALCTODO_H
#include <QObject>
#include "fishcalctodo.h"
#include "fish.h"
#include "predator.h"


class FishCalcToDo;
class Fish;
class QObject;
class foodItem;

class predatorCalcToDo :public FishCalcToDo
{
    Q_OBJECT
public:
    predatorCalcToDo(Fish * infish,QObject * parent = 0);

    void run();
protected:
    void Feed(foodItem *food){}
    void FindFood();
    void Reproduce();
    void decrementSatiety();
    void calcFoodAction();

    bool hunt();
    void eatFish(Fish *fish);
};

#endif // PREDATORCALCTODO_H
