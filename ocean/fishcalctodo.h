#ifndef FISHCALCTODO_H
#define FISHCALCTODO_H

#include <QThread>
#include <QSemaphore>
#include <QList>
#include <QListIterator>
#include <qmath.h>
#include <math.h>
#include "fish.h"
#include "fooditem.h"
//#include "predator.h"
//#include "predatorcalctodo.h"

class Fish;
class foodItem;
class FoodBase;
//class predatorCalcToDo;
extern QSemaphore world_sem;

class FishCalcToDo : public QThread
{
    Q_OBJECT
    friend class Fish;
public:
    explicit FishCalcToDo(Fish * infish,QObject * parent = 0);
    void run();
    quint32 getSemCount() const;
    void calcFoodAction();
    bool FoodExists()const {return isFoodExists;}
protected:
    void Relax();
    void FindFood();
    void Reproduce();
    void Feed(foodItem *food);

    void moveTo(QPointF target,quint32 mulSpeed = 1);
    bool isFoodExists;
//    bool isFoodZone();
//    foodItem * getCurFood();

    bool checkPos(QPointF pos,quint32 error);
    bool isFishNear();
    void calcSpeed();

    void decrementSatiety();
    void Die();
    Fish * m_fish;
    QGraphicsItem *nearestFood;

    bool Stop;
};

#endif // FISHCALCTODO_H
