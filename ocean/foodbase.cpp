#include "foodbase.h"

class FoodBase;
class QObject;

FoodBase::FoodBase(QObject *Parent):QObject(Parent)
{

}

void FoodBase::addFood(QGraphicsItem *food)
{
    base.push_back(food);
}

void FoodBase::removeFood(QGraphicsItem *food)
{
    for(int i = 0 ; i < base.size();i++)
    {
        if(base.at(i) == food)
        {
            base.remove(i);
            break;
        }
    }
}

QGraphicsItem *FoodBase::nearestFoodFrom(QPointF pos)
{
    if(base.isEmpty())return 0;

    qreal disX;
    qreal disY;
    qreal distance;
    qreal minDistance = 0xFFFFFFFF;
    QGraphicsItem* end = 0;
    for(int i = 0 ; i < base.size();i++)
    {
        disX = base.at(i)->pos().x() - pos.x();
        disY = base.at(i)->pos().y() - pos.y();
        distance = qSqrt(disX * disX + disY*disY);
        if(distance < minDistance)
        {
            minDistance = distance;
            end = base.at(i);
        }
    }
    return end;
}

quint32 FoodBase::distanceFrom(QPointF pos1, QPointF pos2)
{
    qreal disX;
    qreal disY;
    qreal distance;

        disX = pos2.x() - pos1.x();
        disY = pos2.y() - pos1.y();
        distance = qSqrt(disX * disX + disY*disY);

    return distance;
}

