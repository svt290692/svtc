#include <QMessageBox>
#include <Box2D/Box2D.h>
#include <QTimer>
#include <QMutex>
#include "foodbase.h"
#include "ocean.h"
#include "ui_ocean.h"
#include "physics_circle.h"
#include "fooditem.h"
#include "fish.h"
#include "predator.h"

static void fillPositions(QVector < QVector < QPoint > > &poses,qint32 columns,qint32 strings,qint32 size)
{
    poses.resize(strings);
    for(int i = 0 ; i < strings;i++)
    {
        poses[i].resize(columns);
        for(int  j = 0 ; j < columns;j++)
        {
            poses[i][j] = QPoint(j * size - (size / 2),i * size - (size / 2) );
        }
    }
}


//extern FoodBase foodItem::baseFood;

Ocean::Ocean(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Ocean)
{
    ui->setupUi(this);
    columnsMax = 300; stringsMax = 300;
    sizeCells = 10;
    zeroCoord.setX(-(ui->GV_ocean->width() / 2));zeroCoord.setY(-(ui->GV_ocean->height() / 2));
    scene = new QGraphicsScene(ui->GV_ocean);

    QBrush br(Qt::cyan);
    br.setColor(Qt::cyan);
    br.setStyle(Qt::Dense3Pattern);
    scene->setBackgroundBrush(br);

    ui->GV_ocean->setScene(scene);
//    scene->setSceneRect(0,0,300,300);
    ui->GV_ocean->setRenderHint(QPainter::Antialiasing);
    ui->GV_ocean->setRenderHint(QPainter::HighQualityAntialiasing);

    fillPositions(positions,columnsMax,stringsMax,sizeCells);

    m_world = new b2World(b2Vec2(0,0));


    QTimer *timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(moveItems()));
    timer->start(1000/60);
    foodItem::baseFood.setParent(this);
//    connect(this,SIGNAL(makeStep()),scene,SLOT(advance()));
//    int i = 0;
    for(int i = 0 ; i < 10; i++)
    new Fish(m_world,scene,-positions[0][i].x(),-positions[0][i].y());

//    for(int i = 0 ; i < 2; i++)
//    new predator(m_world,scene,-positions[0][0].x(),-positions[0][0].y());

    b2BodyDef bd;
    bd.position.Set(0,30);
    bd.type = b2_staticBody;

    b2FixtureDef fd;
    b2PolygonShape ps;
    ps.SetAsBox(50,5);
    fd.shape = &ps;
    b2Body * b =m_world->CreateBody(&bd);
    b->CreateFixture(&fd);
    ui->GV_ocean->scale(0.5,0.5);

    new foodItem(scene,QPoint(200,0));
    new foodItem(scene,QPoint(200,200));
    new foodItem(scene,QPoint(-452,0));
    new foodItem(scene,QPoint(50,-300));


    QPixmap pm(":/other/cursor.png");
    QCursor curs(pm,2,2);
    this->setCursor(curs);

    m_mod = noMode;

}

Ocean::~Ocean()
{
    delete ui;
}

void Ocean::moveItems()
{
    while(world_sem.available() > 0)
    {
        sleepFor(2);
    }

    m_world->Step(1.0f / 60.0f,8,3);

//    emit makeStep();
   scene->advance();
}


void Ocean::on_HS_scale_sliderMoved(int position)
{
    qreal sc = static_cast<float>(position) / 100.0f;
    ui->GV_ocean->resetTransform();
    ui->GV_ocean->scale(sc,sc);
}

void Ocean::on_action_addFish_triggered()
{
    m_mod = addFish;
}

void Ocean::mousePressEvent(QMouseEvent * event)
{
    switch(m_mod)
    {
        case addFish:
        {
            new Fish(m_world,scene,event->pos().x(),event->pos().y());

            break;
        }

    }

    QMainWindow::mousePressEvent(event);
}

