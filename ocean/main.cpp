#include <QApplication>
#include <time.h>
#include <QTime>
#include <QSemaphore>
#include "ocean.h"

QSemaphore world_sem;

int main(int argc, char *argv[])
{
    srand(time(0));

    QApplication a(argc, argv);
    Ocean w;
    w.show();
    
    return a.exec();
}
void sleepFor(qint64 milliseconds)
{
    qint64 timeToExitFunction = QDateTime::currentMSecsSinceEpoch()+milliseconds;

    while(timeToExitFunction > QDateTime::currentMSecsSinceEpoch())
    {
        QApplication::processEvents(QEventLoop::AllEvents, 100);
    }
}
