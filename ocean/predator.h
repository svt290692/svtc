#ifndef PREDATOR_H
#define PREDATOR_H

#include <QGraphicsItem>
#include "fish.h"
#include "predatorcalctodo.h"

class predator : public Fish
{
public:
    predator(b2World * inWorld,QGraphicsScene *inScene,qreal x,qreal y);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    quint32 getEatCount()const{return eatFishCount;}
    void resetEated(){eatFishCount = 0;}

    void incEatCount(){++eatFishCount;}
    void resetEatCount(){eatFishCount = 0;}

    static const quint32 eatToReproduce = 3;
    static const quint32 maxPredatorSatiety = 300;
    static const quint32 rangeView = 100;
protected:
    void advance(int phase);

    void birth();
private:
    quint32 eatFishCount;
};

#endif // PREDATOR_H
