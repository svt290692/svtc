#ifndef PHISICS_GAPHICSITEM_H
#define PHISICS_GAPHICSITEM_H

#include <QGraphicsItem>
#include <QtCore>
#include <QtGui>
#include <Box2D/Box2D.h>


class Phisics_GaphicsItem :public QGraphicsItem
{
public:
    Phisics_GaphicsItem(b2World * inWorld,QGraphicsScene * inScene)
        :m_world(inWorld),m_scene(inScene)
    {
    }

protected:
    b2Body * m_body;
    b2World * m_world;
    QGraphicsScene * m_scene;

};

#endif // PHISICS_GAPHICSITEM_H
