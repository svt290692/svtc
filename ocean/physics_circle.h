#ifndef PHYSICS_ELLIPSE_H
#define PHYSICS_ELLIPSE_H

#include "phisics_gaphicsitem.h"
#include <QPainter>
#include <QGraphicsItem>

//просто кружочек с физикой на сцене
class physics_circle : public Phisics_GaphicsItem
{
public:
    physics_circle(b2World * inWorld,QGraphicsScene *inScene,qreal rad,qreal x,qreal y);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
protected:
    void advance(int phase);
private:
    qreal m_rad;
    QBrush m_brush;
    QPen m_pen;


};

#endif // PHYSICS_ELLIPSE_H
