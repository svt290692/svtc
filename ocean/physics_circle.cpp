#include "physics_circle.h"

physics_circle::physics_circle(b2World *inWorld,QGraphicsScene *inScene, qreal rad, qreal x, qreal y)
    :Phisics_GaphicsItem(inWorld,inScene)
{
    m_rad = rad;
    m_brush = QBrush(Qt::green);
    m_pen = QPen(Qt::black,1,Qt::SolidLine);//,Qt::RoundCap,Qt::RoundJoin);

    b2BodyDef bd;
    bd.position.Set(x,y);
    bd.type = b2_dynamicBody;

    b2CircleShape shape;
    shape.m_radius = m_rad;

    b2FixtureDef fd;
    fd.shape = &shape;

    m_body = m_world->CreateBody(&bd);
    m_body->CreateFixture(&fd);

    m_scene->addItem(this);
}

QRectF physics_circle::boundingRect() const
{
    return QRectF(-(m_rad),-(m_rad),m_rad * 2,m_rad * 2);
}

void physics_circle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(m_pen);
    painter->setBrush(m_brush);
    painter->drawEllipse(boundingRect());
}

void physics_circle::advance(int phase)
{
    if(!phase) return;

    setRotation(m_body->GetAngle());
    setPos(m_body->GetPosition().x,m_body->GetPosition().y);
}
