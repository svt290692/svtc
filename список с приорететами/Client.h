#pragma once
#include<time.h>
#include<iostream>
class Client
{
public:

	Client(void)
	{
	
		size_t who = std::rand()%100;
		
		if(who == 99)m_importance = director;
		else if(who < 40)m_importance = tramp;
		else if(who < 75)m_importance = citizen;
		else if(who < 90)m_importance = vip;
		else m_importance = chief;

		needTime = ( std::rand() ) % 2000;
	}
	clock_t NeedTime() {return needTime;}
	size_t priority(){return m_importance;}
private:
	clock_t needTime;
	enum importance
	{
		tramp,
		citizen = 10,
		client = 20,
		vip = 50,
		chief = 2000,
		director = 999999
	} m_importance;

};

