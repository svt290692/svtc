#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "maze_generator.h"
#include "mazeserver_widget.h"
#include <QGridLayout>
#include <QPushButton>
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_MazeServer = new MazeServer_Widget(this);
   m_MazeServer->show();
   resize(800,600);
   m_MazeServer->resize(800,600);

   connect(m_MazeServer,SIGNAL(sock_common(QTcpSocket*)),this,SLOT(inSocket(QTcpSocket*)));

}

MainWindow::~MainWindow()
{
    delete ui;/*
    if(m_maze) delete m_maze;*/
}

//void MainWindow::set_maze(maze_generator maze)
//{/*
//    if(m_maze) delete m_maze;
//    m_maze = new maze_generator(maze);*/
//}

//void MainWindow::draw_maze()
//{

//}

void MainWindow::on_A_createMze_triggered()
{
    maze_options *options = new maze_options(this);
    options->show();

    connect(options,SIGNAL(sig_maze(maze_generator)),m_MazeServer,SLOT(setMaze(maze_generator)));

}

void MainWindow::inSocket(QTcpSocket *sock)
{
    mes_statusBar_sock(sock,"New Socket connected ");
}

void MainWindow::sockError(QTcpSocket *sock)
{
    mes_statusBar_sock(sock,QString::fromUtf8("сокет больше не валиден"));
}

void MainWindow::mes_statusBar_sock(QTcpSocket *sock, QString mess)
{
    QString nSock(mess);

    nSock += "Addres :<";
    nSock +=  sock->localAddress().toString();
    nSock += "> ";

    nSock += "Port :<";
    nSock +=  QString::number(sock->localPort());
    nSock += ">";

    ui->statusBar->showMessage(nSock);
}

void MainWindow::keyPressEvent(QKeyEvent *key)
{
    switch(key->key())
    {
        case Qt::Key_Minus: m_MazeServer->m_scale -= 0.1; break;
    case Qt::Key_Plus:case Qt::Key_Equal:  m_MazeServer->m_scale += 0.1; break;
        default: QWidget::keyPressEvent(key); break;
    }
}

