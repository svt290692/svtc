#ifndef MAZE_GENERATOR_H
#define MAZE_GENERATOR_H
#include <QDialog>
#include <exception>
#include <stdlib.h>
#include <QVector>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include "maze_node.h"

class maze_generator
{
public:
    maze_generator(qint32 inColumns, qint32 inStrings);
    maze_generator(maze_generator &other);
    ~maze_generator();
    void generate(qint32 seed);
    void record_to_file(const QString& f_name);

    const qint32 columns;
    const qint32 strings;

    const maze_node* operator[](qint32 node);
    const maze_node& at(qint32 y,qint32 x);

private:
    maze_node **m_maze;
    void alert();
};

#endif // MAZE_GENERATOR_H
