#ifndef MAZE_OPTIONS_H
#define MAZE_OPTIONS_H

#include <QDialog>
#include <QAbstractButton>
#include <QMessageBox>
#include <QRegExp>
#include "maze_generator.h"

namespace Ui {
class maze_options;
}


class maze_options : public QDialog
{
    Q_OBJECT
    
public:
    explicit maze_options(QWidget *parent = 0);
    ~maze_options();
private slots:
    void on_PB_ok_clicked();
    void on_PB_close_clicked();

    void on_PB_reset_clicked();

signals:
    void sig_maze(maze_generator maze);
private:
    Ui::maze_options *ui;
};

#endif // MAZE_OPTIONS_H
