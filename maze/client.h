#ifndef CLIENT_H
#define CLIENT_H

#include <QTcpSocket>
#include <QMessageBox>
#include <QByteArray>
#include <QDataStream>
#include <QObject>
class Maze_Client :QObject
{
    Q_OBJECT
public:
    Maze_Client(QTcpSocket * socket,int x = 0,int y = 0,QObject *parent = 0):QObject(parent)
    {
        this->m_pos.x = x; this->m_pos.y = y;
        this->socket = socket;

        connect(socket,SIGNAL(readyRead()),this,SLOT(DataResive()));
    }
    Maze_Client(const Maze_Client &other):QObject(other.parent())
    {
        m_pos = other.m_pos;
        socket = other.socket;
        connect(socket,SIGNAL(readyRead()),this,SLOT(DataResive()));
    }

    struct pos
    {
        int x;
        int y;
    }m_pos;
    QTcpSocket *socket;
public slots:
    void DataResive()
    {
        if(!socket->isReadable() || !socket->isValid())
            return;
//        if(!socket->waitForReadyRead(3000))
//        {
//            QMessageBox::information(0,"error","error socket read");
//            return;
//        }
        QDataStream stream(socket);

        stream >> m_pos.y;
        stream >> m_pos.x;
    }
};

#endif // CLIENT_H
