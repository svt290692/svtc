#ifndef MAZESERVER_WIDGET_H
#define MAZESERVER_WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QList>
#include <QGraphicsWidget>
#include <QMessageBox>
#include <QGraphicsView>
#include <QTcpServer>
#include <QDataStream>
#include <QTcpSocket>
#include "maze_generator.h"
#include "client.h"



class MazeServer_Widget : public QWidget
{
    Q_OBJECT
public:
    explicit MazeServer_Widget(QWidget *parent = 0);
    ~MazeServer_Widget();
    void sendMazeToAll();
    qreal m_scale;
signals:
    void sock_common(QTcpSocket * sock);
    void sock_notValid(QTcpSocket * sock);
public slots:
    void setMaze(maze_generator maze);
    void sendMaze(Maze_Client & client);
    void newConnection();
protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent * key);
private:
        maze_generator *m_maze;
        QTcpServer *server;
        QList<Maze_Client> m_clients;
};

#endif // MAZESERVER_WIDGET_H
