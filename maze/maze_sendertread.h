#ifndef MAZE_SENDERTREAD_H
#define MAZE_SENDERTREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDataStream>
#include <QByteArray>
#include "maze_generator.h"

class maze_senderTread : public QThread
{
    Q_OBJECT
public:
    explicit maze_senderTread(maze_generator *maze, QTcpSocket *sock, QObject *parent = 0);
    void run();
signals:
    
public slots:
private:
    maze_generator *m_maze;
    QTcpSocket *m_sock;
};

#endif // MAZE_SENDERTREAD_H
