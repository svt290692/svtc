#include "maze_generator.h"
#include<time.h>
#include <QMessageBox>
maze_generator::maze_generator(qint32 inColumns,qint32 inStrings):columns(inColumns),strings(inStrings)
{
    m_maze = new maze_node*[strings];

    for(int i = 0; i < strings; i++)
    {
        m_maze[i] = new maze_node[columns];
        memset(m_maze[i],0,sizeof(maze_node)*columns);
    }
}

maze_generator::maze_generator(maze_generator &other):strings(other.strings),columns(other.columns)
{
    m_maze = new maze_node*[strings];

    for(int i = 0; i < strings; i++)
    {
        m_maze[i] = new maze_node[columns];
        memcpy(m_maze[i],other.m_maze[i],sizeof(maze_node) * columns);
    }
}

maze_generator::~maze_generator()
{
    for(int i = strings - 1; i >= 0;i--)
    {
        delete[] m_maze[i];
    }
    delete[] m_maze;
}
static int choice_string_set(maze_node* string,qint32 size,qint32 ind = 0)
{
    qint32 choice = -1;
    QVector<qint32> nums;
    for(int i = 0 ; i < size; i ++)
    {
        nums.push_back(string[i].m_set);
    }
    for(int i = ind ; i < 0xFFFFFFF;i++)
    {
        bool isit = true;
        for(int j = 0 ; j < nums.size();j++)
        {
            if(i == nums[j])
            {
                isit = false;
                break;
            }
        }
        if(isit == true)
        {
            choice = i;
            break;
        }
    }
    return choice;
}
void maze_generator::generate(qint32 seed)
{
    srand(seed);
    // заполнение крайних стенок
    for(int i = 0; i < strings;i++)
    {
        m_maze[i][columns-1].wal_r = true;
    }
    // присваиваем свое множество каждому элементу первой строчки
    for(int i = 0; i < columns;i++)
    {
        m_maze[0][i].m_set = i; 
        m_maze[0][i].wal_b = true;
    }
    for(int i = 0; i < columns-1;i++)
    {
            if( (rand() % 10) < 5 )// обьеденить или добавить стеночку
            {
                m_maze[0][i+1].m_set  = m_maze[0][i].m_set;
                m_maze[0][i].wal_r  = false;
            }
            else
            {
                m_maze[0][i].wal_r  = true;
            }
    }
    {
    qint32 set_offset = 0;
    qint32 set_begin = 0;
    qint32 last_s = m_maze[0][0].m_set;
    for(int i = 0; i < columns;i++)//для каждого множества оставить как минимум одну нижнюю границу
    {
        if(i == columns - 1 && last_s == m_maze[0][i].m_set)
        {
            ++set_offset;

            qint32 sum_botFalse = (rand() % set_offset);// в пределах одного множество нужно убрать минимум одну нижнюю границу
            qint32 beg_cell = set_begin + (rand() % (set_offset - sum_botFalse) );// тут расчет того как много их нужно убрать
            // в цикле убираем нижние границы у случайного количества ячеек одного множества
            for(int j = beg_cell; j <= (beg_cell + sum_botFalse); j++)
            {
            qint32 ind = j; //(set_begin + (rand() % (set_offset)));
            m_maze[0][ind].wal_b = false;
            }
        }
        else
        {
        if(last_s == m_maze[0][i].m_set)
            ++set_offset;
        else
        {

            qint32 sum_botFalse = (rand() % set_offset);// в пределах одного множество нужно убрать минимум одну нижнюю границу
            qint32 beg_cell = set_begin + (rand() % (set_offset - sum_botFalse) );// тут расчет того как много их нужно убрать
            // в цикле убираем нижние границы у случайного количества ячеек одного множества
            for(int j = beg_cell; j <= (beg_cell + sum_botFalse); j++)
            {
            qint32 ind = j; //(set_begin + (rand() % (set_offset)));
            m_maze[0][ind].wal_b = false;
            }
            set_begin = i;
            set_offset = 1;
            if(i == columns-1 && last_s != m_maze[0][i].m_set)
            {
                m_maze[0][columns - 1 ].wal_b = false;
            }
        }
        last_s = m_maze[0][i].m_set;
        }
    }
    }
    ///////////////

    for(int i = 1; i < strings;i++)
    {

        for(int j = 0; j < columns;j++)// копировать врехнюю строчку вниз
        {
            m_maze[i][j].m_set = m_maze[i-1][j].m_set;
            m_maze[i][j].wal_b = m_maze[i-1][j].wal_b;
            m_maze[i][j].wal_r = m_maze[i-1][j].wal_r;
        }

        for(int j = 0; j < columns;j++)
        {

            if( j < columns-1 )// удалить все правые границы
            {
                m_maze[i][j].wal_r = false;
            }

            // удалить из множеств те ячейки где есть нижние границы
            if(m_maze[i][j].wal_b)
            {
                m_maze[i][j].m_set = -1;
                m_maze[i][j].wal_b = false; // а потом и сами нижние границы
            }
        }

        qint32 LS = m_maze[i][0].m_set;
            for(int j = 0; j < columns;j++)
            {//добавить ячейкам без множеств свои уникальный множества
                if(m_maze[i][j].m_set == -1)
                {
                    qint32 num = choice_string_set(m_maze[i],columns,LS);
                    m_maze[i][j].m_set = num;
                    LS = num;
                }
                else LS = m_maze[i][j].m_set;
            }
            for(int j = 0; j < columns-1;j++)
            {// обьеденить или добавить границу
                if(m_maze[i][j].m_set == m_maze[i][j+1].m_set)
                {
                    m_maze[i][j].wal_r = true;
                }
                else if((rand() % 10) < 5 )// обьеденить или добавить стеночку
                {
                    m_maze[i][j+1].m_set  = m_maze[i][j].m_set;
                    m_maze[i][j].wal_r  = false;

                }
                else
                {
                    m_maze[i][j].wal_r  = true;
                }
            }

            for(int j = 0; j < columns;j++)
            {
                m_maze[i][j].wal_b = true;
            }

            qint32 set_offset = 0;
            qint32 set_begin = 0;
            qint32 last_s = m_maze[i][0].m_set;
            for(int j = 0; j < columns;j++)//для каждого множества оставить тоько одну нижнюю границу
            {
                if(j == columns - 1 && last_s == m_maze[i][j].m_set)
                {
                    ++set_offset;

                    qint32 sum_botFalse = (rand() % set_offset);// в пределах одного множество нужно убрать минимум одну нижнюю границу
                    qint32 beg_cell = set_begin + (rand() % (set_offset - sum_botFalse) );// тут расчет того как много их нужно убрать
                    // в цикле убираем нижние границы у случайного количества ячеек одного множества
                    for(int k = beg_cell; k <= (beg_cell + sum_botFalse); k++)
                    {
                    qint32 ind = k; //(set_begin + (rand() % (set_offset)));
                    m_maze[i][ind].wal_b = false;
                    }
                }
                else
                {
                if(last_s == m_maze[i][j].m_set)
                    ++set_offset;
                else
                {

                    qint32 sum_botFalse = (rand() % set_offset);// в пределах одного множество нужно убрать минимум одну нижнюю границу
                    qint32 beg_cell = set_begin + (rand() % (set_offset - sum_botFalse) );// тут расчет того как много их нужно убрать
                    // в цикле убираем нижние границы у случайного количества ячеек одного множества
                    for(int k = beg_cell; k <= (beg_cell + sum_botFalse); k++)
                    {
                    qint32 ind = k; //(set_begin + (rand() % (set_offset)));
                    m_maze[i][ind].wal_b = false;
                    }
                    set_begin = j;
                    set_offset = 1;
                    if(j == columns-1 && last_s != m_maze[i][j].m_set)
                    {
                        m_maze[i][columns - 1 ].wal_b = false;
                    }
                }
                last_s = m_maze[i][j].m_set;
                }
            }
//    alert();
        }

//    for(int j = 0; j < columns;j++)// копировать врехнюю строчку вниз
//    {
//        m_maze[strings-1][j].m_set = m_maze[strings - 2 ][j].m_set;
//        m_maze[strings-1][j].wal_r = m_maze[strings - 2 ][j].wal_r;
//    }
        for(int j = 0; j < columns-1; j++)
        {
            // удаление стеночек от одинаковых множеств
            if(m_maze[strings-1][j].m_set != m_maze[strings-1][j+1].m_set)
            {
                m_maze[strings-1][j].wal_r = false;
            }
        }
        for(int i = 0; i < columns;i++)
        {
            m_maze[strings-1][i].wal_b = true;
        }

//        alert();
}

void maze_generator::record_to_file(const QString &f_name)
{
    QFile file(f_name);

    if(!file.open(QFile::WriteOnly))
    {
        QMessageBox M_B;
        M_B.setWindowTitle("Error");
        M_B.setText("Error File Open to Write");
        M_B.exec();
        return;
    }
    QTextStream T_S(&file);

    for(int i = 0 ; i < strings;i++)
    {
      file.write(" ");  file.write("__");
    }
    file.write(" "); file.write("\n");

    for(int i = 0 ; i < strings;i++)
    {
        file.write("|");
        for(int j = 0 ; j < columns;j++)
        {
            if(m_maze[i][j].wal_b)
            file.write("__");
            else file.write("  ");

            if(m_maze[i][j].wal_r)
            file.write("|");
            else file.write(" ");
        }
        file.write("\n");
    }
file.close();

}


const maze_node* maze_generator::operator [](int node)
{
    if(node < columns)
    {
        return m_maze[node];
    }
    else throw std::exception();
}

const maze_node &maze_generator::at(qint32 y, qint32 x)
{
    if(x >= columns || y >= strings) throw std::exception();

    return m_maze[y][x];
}

void maze_generator::alert()
{
    QMessageBox mb;
    QString str;

    for(int i = 0 ; i < strings;i++)
    {
        for(int j = 0 ; j < columns;j++)
        {
            str += QString::number(m_maze[i][j].m_set);
            str += '|';
        }
        str += '\n';
    }
    mb.setText(str);
    mb.exec();
}


