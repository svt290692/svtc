#include "mazeserver_widget.h"
#include <QPainter>
#include <QByteArray>
#include <QBitArray>
#include <QDataStream>
#include <QMainWindow>
#include "maze_sendertread.h"

MazeServer_Widget::MazeServer_Widget(QWidget *parent) :
    QWidget(parent)
{
    m_maze = 0;
    m_scale = 0.5;
    server = new QTcpServer(this);
    QTimer *timer = new QTimer(this);

    connect(timer,SIGNAL(timeout()),this,SLOT(update()));
   // connect(timer,SIGNAL(timeout()),this,SLOT(sendMaze()));
    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    timer->start(300);

    if(!server->listen(QHostAddress::Any,2906))
    {
        QMessageBox::information(this,"error","Can not listen");
    }
}

MazeServer_Widget::~MazeServer_Widget()
{
    if(m_maze) delete m_maze;
}

void MazeServer_Widget::sendMazeToAll()
{
    foreach(Maze_Client client,m_clients)
    {

        if(!client.socket->isValid())
        {
            emit sock_notValid(client.socket);
           client.socket->close();
           client.socket = 0;
           continue;
        }

        if(client.socket == 0 || !(client.socket->isWritable()))
        {
//            QMessageBox::information(this,"sock error",QString::fromUtf8("сокет для отправки лабиринта не валиден (мать его)"));
            continue;
        }
        sendMaze(client);

    }

        for(QList<Maze_Client>::Iterator it = m_clients.begin();it != m_clients.end();it++ )
        {
            if(it->socket == 0) m_clients.erase(it);
        }
}

void MazeServer_Widget::setMaze(maze_generator maze)
{
    if(m_maze) delete m_maze;
    m_maze = new maze_generator(maze);
}

void MazeServer_Widget::sendMaze(Maze_Client &client)
{
    if(client.socket == 0 || !(client.socket->isWritable()))
    {
        QMessageBox::information(this,"sock error",QString::fromUtf8("сокет для отправки лабиринта не валиден (мать его)"));
        return;
    }
//    maze_senderTread sender(m_maze,client.socket,this);
//    sender.start();
    QDataStream stream(client.socket);

    stream << m_maze->columns;
    stream << m_maze->strings;
    for(int i = 0; i < m_maze->strings;i++)
        for(int j = 0; j < m_maze->columns;j++)
        {
            quint8 int_NodeToSend = 0;
            if(m_maze->at(i,j).wal_b)
                int_NodeToSend |= 1;

            if(m_maze->at(i,j).wal_r)
                int_NodeToSend |= 2;

            stream << int_NodeToSend;
        }
}

void MazeServer_Widget::newConnection()
{
    QTcpSocket *socket = server->nextPendingConnection();
    Maze_Client newClient(socket);
    m_clients.push_back(newClient);
    sendMaze(newClient);

    emit sock_common(socket);
}

void MazeServer_Widget::paintEvent(QPaintEvent *event)
{
  // setGeometry((static_cast<QMainWindow*>(parent())->geometry()));
    if(!m_maze)
        return;
    const  qreal max_cell_size = qMin(width(),height())-100;

    qreal size_cell =  max_cell_size / qMax(m_maze->columns,m_maze->strings);
    QPoint zero(0,0);
    QColor wall_color(186,85,211);

    QPainter paint(this);

    paint.setRenderHint(QPainter::Antialiasing);

    paint.scale(m_scale,m_scale);


    QPen pen;
    pen.setBrush(wall_color);
    pen.setColor(wall_color);
    pen.setWidth(size_cell * 0.1);
    paint.setPen(pen);
    paint.setBrush(wall_color);
    paint.setBackground(wall_color);


    paint.drawLine(QPoint(size_cell,size_cell),QPoint(size_cell * m_maze->columns + size_cell,size_cell));
    paint.drawLine(QPoint(size_cell,size_cell),QPoint(size_cell,size_cell * m_maze->strings + size_cell));
    for(int i = 0 ; i < m_maze->strings;i++)
    {
        paint.translate(size_cell * 2,((i+2) * size_cell));
        for(int j = 0 ; j < m_maze->columns;j++)
        {
            if(m_maze->at(i,j).wal_r) paint.drawLine(zero,QPoint(0,-size_cell));
            if(m_maze->at(i,j).wal_b) paint.drawLine(zero,QPoint(-size_cell,0));
            paint.translate(size_cell,0);
        }

        paint.resetTransform();
        paint.scale(m_scale,m_scale);
    }

    foreach(Maze_Client client,m_clients)
    {
        paint.translate(((size_cell * 2) + (client.m_pos.x * size_cell)),(client.m_pos.y + 2) * size_cell);
        paint.drawEllipse( -(size_cell * 0.5), -(size_cell * 0.5), (size_cell * 0.25),(size_cell * 0.25));

        paint.resetTransform();
        paint.scale(m_scale,m_scale);

    }

}

void MazeServer_Widget::keyPressEvent(QKeyEvent *key)
{
    switch(key->key())
    {
        case Qt::Key_Minus: m_scale -= 0.5; break;
        case Qt::Key_Plus:case Qt::Key_Equal:  m_scale += 0.5; break;
        default: QWidget::keyPressEvent(key); break;
    }
    update();
}
