#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "maze_generator.h"
#include "maze_options.h"
#include "mazeserver_widget.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void on_A_createMze_triggered();
    void inSocket(QTcpSocket * sock);
    void sockError(QTcpSocket * sock);
//    void set_maze(maze_generator maze);
protected:
    void keyPressEvent(QKeyEvent *key);
private:
    void mes_statusBar_sock(QTcpSocket * sock,QString mess);
    Ui::MainWindow *ui;
    MazeServer_Widget *m_MazeServer;
};

#endif // MAINWINDOW_H
