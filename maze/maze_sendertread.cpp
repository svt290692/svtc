#include "maze_sendertread.h"

maze_senderTread::maze_senderTread(maze_generator *maze, QTcpSocket *sock, QObject *parent):  QThread(parent)
{
    m_maze = maze;
    m_sock = sock;
}
void maze_senderTread::run()
{
    QDataStream stream(m_sock);

    stream << m_maze->columns;
    stream << m_maze->strings;
    for(int i = 0; i < m_maze->strings;i++)
        for(int j = 0; j < m_maze->columns;j++)
        {
            quint8 int_NodeToSend = 0;
            if(m_maze->at(i,j).wal_b)
                int_NodeToSend |= 1;

            if(m_maze->at(i,j).wal_r)
                int_NodeToSend |= 2;

            stream << int_NodeToSend;
        }
}


