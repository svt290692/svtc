#include "maze_options.h"
#include "ui_maze_options.h"
#include <QTime>
#include <QDate>

maze_options::maze_options(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::maze_options)
{
    ui->setupUi(this);

    ui->RB_random->setChecked(true);
    ui->PB_ok->setDefault(true);
}

maze_options::~maze_options()
{
    delete ui;
}

// создаем лабиринт и передаем сигнал с ним
void maze_options::on_PB_ok_clicked()
{
    if(ui->LE_size_x->text().isEmpty())
    {
        QMessageBox::information(this,"",QString::fromUtf8("не заполнено поле <Количество столбцов лабиринта>"));
        return;
    }
    if(ui->LE_size_y->text().isEmpty())
    {
        QMessageBox::information(this,"",QString::fromUtf8("не заполнено поле <Количество строчек лабиринта>"));
        return;
    }

    if(ui->LE_size_y->text().toInt() < 4 || ui->LE_size_x->text().toInt() < 4 || ui->LE_size_y->text().toInt() > 255 || ui->LE_size_x->text().toInt() > 255 )
    {
        QMessageBox::information(this,"",QString::fromUtf8("указан лабиринт неправильного размера\n нужно как минимум по 4 ячейки туда и туда\n и не более 255"));
        return;
    }

    maze_generator maze(ui->LE_size_x->text().toInt(),ui->LE_size_y->text().toInt());
    if(ui->RB_fromSeed->isChecked())
        maze.generate(ui->LE_seed->text().toInt());
    else maze.generate(QTime::currentTime().toString("sshhmm").toInt());

    QString name_file("mazeGen");
    if(ui->RB_fromSeed->isChecked())
    {
        name_file += "_FromSeed";
        name_file += ui->LE_seed->text();
    }
    else
    {
        name_file += "_random";
        name_file += QDate::currentDate().toString("\"mmdd \"");
        name_file += QTime::currentTime().toString("hh::mm");
    }
    maze.record_to_file(name_file);
    maze.record_to_file("test_maze");
    emit sig_maze(maze);
    close();
}

void maze_options::on_PB_close_clicked()
{
    close();
}

void maze_options::on_PB_reset_clicked()
{
    ui->LE_seed->setText("");
    ui->LE_size_x->setText("");
    ui->LE_size_y->setText("");
    ui->RB_fromSeed->setChecked(false);
    ui->RB_random->setChecked(true);
}
