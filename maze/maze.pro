#-------------------------------------------------
#
# Project created by QtCreator 2014-05-08T11:10:14
#
#-------------------------------------------------

QT       += core gui
QT       += core network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = maze
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    maze_options.cpp \
    maze_generator.cpp \
    mazeserver_widget.cpp \
    maze_sendertread.cpp

HEADERS  += mainwindow.h \
    maze_options.h \
    maze_generator.h \
    maze_node.h \
    mazeserver_widget.h \
    client.h \
    maze_sendertread.h

FORMS    += mainwindow.ui \
    maze_options.ui
