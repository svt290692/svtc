#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <Windows.h>
#include <conio.h>
#define UTIL_CCCR(num,i) num & (1<<(i-1))
#define UTIL_CHARGE(num,i) num = num | (1<<(i-1))
#define UTIL_RELIVE(num,i) if(num & (1<<(i-1)))num = (num ^ (1<<(i-1))); else;

#define _BIT_VALID(xBitMask,xVar) ((xBitMask) & (1 << (xVar)))
#define _BIT_ADD(xBitMask,xVar) ((xBitMask) |= (1 << (xVar)))
#define _BIT_SUB(xBitMask,xVar) ((xBitMask) &= ~(1 << (xVar)))
#define _BIT_NOT_VALID(xBitMask,xVar) (~(xBitMask) & (1 << (xVar)))

#define _FLAG_VALID(xBitMask,xVar) ((xBitMask) & (xVar))
#define _FLAG_ADD(xBitMask,xVar) ((xBitMask) |= (xVar))
#define _FLAG_SUB(xBitMask,xVar) ((xBitMask) &= ~(xVar))
#define _FLAG_NOT_VALID(xBitMask,xVar) (~(xBitMask) & (xVar))
#define GET_TACT __rdtsc()

int UTIL_randomNum(int _iMin, int _iMax);
bool UTIL_chance(unsigned char _cPercent);
void UTIL_swapper(int &a,int &b);
void chomp(char buf[]);
char *rus_text(char *from, char *to);
void set_color_ground_console(HANDLE descript_out,int text,int background);
void rus_text_printf_string(char *from);