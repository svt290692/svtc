#include <glut.h>
#include "field.h"
#ifndef ___FIG___
#define ___FIG___

class Figures : public Field
{
public:
	Figures(GLint size_width,GLint size_height);
	Figures();

	int create_figure();
	void draw_game();
	void draw_figur(COORD*);
	void step_game();
	void move_left();
	void move_right();
	void turn();
	int get_FPS()const;
	int get_time_step()const;
	void set_time_step(int);
private:
	int check_line();
	void del_line(int);
	void unactive_fig();
	void move_down();
	void del_fig();
	void restore_fig();
	enum
	{
		qube,
		g_shape,
		stick,
		z_shape
	};
	COORD coords_current_shape[4];
	int FPS;
	int time_per_step;
	int check_act_fig();
	short turn_shape;
	int current_shape;
	int data_mas_fig[20][10];
	bool activ_fig;
	int SPACE;
	int ACTIVE_FIGURE;
	int PASSIVE_FIGURE;
};
#endif