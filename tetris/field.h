#include <glut.h>
#include <windows.h>

#ifndef __FIELD__
#define __FIELD__

class Field
{
public:
Field::Field(GLint size_width,GLint size_height);
Field::Field(){};

	void draw();
	void draw_test_field();
	void set_size(GLint width, GLint height);
protected:
	void fill_mass_coords(COORD (*mass_y)[10])const; // ������� ���������� ������� ����������� � ������������ �������� ��� ���������
	COORD mass_coord_qubes[20][10]; 
	GLint size_qube;
	GLint game_field_width;
	GLint game_field_height;
	GLint full_size_width;
	GLint full_size_height;



};

#endif